<?php
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

$user->session_begin();
$auth->acl($user->data);
$user->setup();

$username = $request->variable('username', '');
$password = $request->variable('password', '');
$ip = $request->variable('ip', '');

$user->ip = $ip;

$result = $auth->login($username, $password);

$resultHeader = 'login_result: ' . $result['status'];
header($resultHeader);
if ($result['status'] == 3) {
  $idHeader = 'user_id: ' . $user->data['user_id'];
  $usernameHeader = 'user_name: ' . $user->data['username'];
  header($idHeader);
  header($usernameHeader);
}
