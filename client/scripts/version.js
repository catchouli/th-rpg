'use strict';

const clientVersion = '1.5.1';

if (typeof exports !== 'undefined')
  exports.version = clientVersion;
