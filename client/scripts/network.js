var pingInterval;

socket.on('login_failed', function(data) {
  loginError(data.error);
});

socket.on('login_success', function(data) {
  Crafty.enterScene('Game');
  sessionBegin();
});

socket.on('connect', function() {
  pingInterval = setInterval(function() {
    socket.emit('pling', {});
  }, 1000);
});

function loggedOut(msg) {
  // for some reason changing scenes wasn't destroying the terrain entity
  Crafty('*').destroy();
  Crafty.enterScene('Login');
  sessionEnd();
  loginError(msg);
  $('#tools-buttons-select').click();
}

socket.on('disconnect', function() {
  clearInterval(pingInterval);
  loggedOut('Lost connection to server');
});

socket.on('force_disconnect', function(data) {
  loggedOut(data.error);
});

// Handle any player joins, including local ones, by creating a player entity
socket.on('player_joined', function(data) {
  console.log('new player: ' + data.user_id);

  var player = Crafty.e('Character')
    .attr({x: data.playerState.pos.x, y: data.playerState.pos.y, w: 32, h: 48, z: 99})
    .origin('center');

  // tag player with user id
  player.addComponent(data.user_id);

  // Add local or remote character component
  if (data.local) {
    console.log('creating editor');
    Crafty.e('Editor');

    // now we're logged in, create the world
    let terrain = Crafty.e('Terrain');
    if (typeof data.playerState.dimension != 'undefined')
      terrain.dimension = data.playerState.dimension;

    player.addComponent("LocalCharacter");
    // Local character should always be drawn on top
    player.z = 100;

    // An entity that does stuff to do with chat
    Crafty.e('Chat, Keyboard').bind('KeyDown', function(data) {
      if (data.originalEvent.code == 'Enter')
        $('#m').focus();
    });
  }
  else {
    player.addComponent('RemoteCharacter');
  }

  // update initial player state
  player.trigger('RemotePlayerUpdate', data);
});

// Destroy player entity when player leaves
socket.on('player_left', function(data) {
  console.log('remote player ' + data.user_id + ' disconnected');
  var e = Crafty(data.user_id);
  e.destroy();
});

// Handle player move events
socket.on('player_update', function(data) {
  var e = Crafty(data.user_id);
  e.trigger('RemotePlayerUpdate', data);
});

// Handle messages from players
socket.on('player_message', function(data) {
  var e = Crafty(data.user_id);
  e.trigger('NewMessage', {msg: data.message, priv: false});

  var msgText = data.playerName + ': ' + data.message;
  addToChatLog(msgText);
});

// Handle private messages from players
socket.on('private_message', function(data) {
  var e = Crafty(data.user_id);
  e.trigger('NewMessage', {msg: data.message, priv: true});

  var msgText = '[priv] ' + data.playerName + ': ' + data.message;
  addToChatLog(msgText);
});

// Handle messages from server
socket.on('server_message', function(data) {
  addToChatLog(data.message);
});

// Handle placing blocks
socket.on('place_block', function(data) {
  Crafty('Editor').trigger('PlaceBlock', data);
});

// Handle clearing blocks
socket.on('clear_block', function(data) {
  Crafty('Editor').trigger('ClearBlock', data);
});

// Handle new chunk data
socket.on('chunk_data', function(data) {
  Crafty('Terrain').trigger('NewChunkData', data);
  Crafty('LocalCharacter').trigger('NewChunkData', data);
});

// Handle getting the chunk owner
socket.on('get_chunk_owner', function(data) {
  Crafty('LocalCharacter').trigger('GetChunkOwner', data);
});
