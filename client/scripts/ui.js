$('#tools').mousedown(function(e) { e.stopPropagation(); });
$('#tools').mousemove(function(e) { e.stopPropagation(); });
$('#lock').mousedown(function(e) { e.stopPropagation(); });

$('#lock').mouseenter(function(e) {
  $('#locked_name').css('display', 'initial');
  $('#lock').attr('mouseover', 'true');
  Crafty('LocalCharacter').trigger('NewChunkData', {});
});
$('#lock').mouseleave(function(e) {
  $('#locked_name').css('display', 'none');
  $('#lock').attr('mouseover', '');
  Crafty('LocalCharacter').trigger('NewChunkData', {});
});
$('#lock').mousemove(function(e) {
  $('#locked_name').css('top', e.clientY);
  $('#locked_name').css('left', e.clientX);
  e.stopPropagation();
});
$('#lock, #locked_name').click(function() {
  console.log('clicked lock');
  socket.emit('toggle_chunk_owner', {});
});

// Character sprites
// Currently unused
socket.on('character_sprite', function(data) {
  var newElement = $('<a/>', {
    style: "background-image: url('sprites/characters/001-Fighter01.png')",
    class: "character-selector"
  }).appendTo('#character-select');
});

// Hide ui elements depending on screen size
function onResize() {
  if (window.innerWidth < 750 || window.innerHeight < 350) {
    $('#ui').css('display', 'none');
  }
  else {
    $('#ui').css('display', 'initial');
  }
}
$(window).resize(onResize);
onResize();

// Notify parent window if we gain or lose focus
$(window).focus(function() {
  console.log('gained focus');
  setTimeout(scrollChatToBottom, 1000);
  if (parent) {
    parent.postMessage({type: 'gained_focus'}, "*");
  }
});
$(window).blur(function() {
  console.log('lost focus');
  if (parent) {
    parent.postMessage({type: 'lost_focus'}, "*");
  }
});
