'use strict';

Crafty.c("Character", {
  required: "2D, Canvas, SpriteAnimation, Motion, Delay",

  // Initialise
  init: function() {
    // Initial player state
    this.playerState = {};

    // Create player name entity
    this.playerName = Crafty.e('2D, Canvas, Text')
      .attr({x: this.x+16, y: this.y-16, z: 1000})
      .textAlign('center')
      .textFont({ size: '14px', weight: 'bold' })
      .textColor('#F0F0B0');
    this.attach(this.playerName);

    // Create player flair entity
    this.playerFlair = Crafty.e('2D, Canvas, Text')
      .attr({x: this.x+16, y: this.y-32, z: 1000})
      .textAlign('center')
      .textFont({ size: '12px', weight: 'bold' })
      .textColor('#D3D3A0');
    this.attach(this.playerFlair);

    // Create player message entity
    this.playerMessage = Crafty.e('2D, Canvas, Text')
      .attr({x: this.x+16, y: this.y-48, z: 1001})
      .textAlign('center')
      .textFont({ size: '14px', weight: 'bold' });
    this.attach(this.playerMessage);

    // Set fields up
    this.spritesLoaded = false;
  },

  // Load an image and set up reels
  charset: function(filename) {
    var thiz = this;

    // Load spritesheet
    this.spritesLoaded = false;
    Crafty.load(
    {
      "sprites": {
        [filename]: {
          tile: 32,
          tileh: 48,
          map: {
            [filename]: [0, 0]
          }
        }
      }
    },
    function() {
      if (thiz.curSprite)
        thiz.removeComponent(thiz.curSprite);
      thiz.curSprite = filename;
      thiz.addComponent(filename);

      thiz.reel("walk-down",  500, [ [0, 0], [1, 0], [2, 0], [3, 0] ]);
      thiz.reel("walk-left",  500, [ [0, 1], [1, 1], [2, 1], [3, 1] ]);
      thiz.reel("walk-right", 500, [ [0, 2], [1, 2], [2, 2], [3, 2] ]);
      thiz.reel("walk-up",    500, [ [0, 3], [1, 3], [2, 3], [3, 3] ]);

      thiz.animationSpeed = 0;
      thiz.animate("walk-down", -1);

      thiz.spritesLoaded = true;
    });

    return this;
  },

  clearMessage: function() {
    this.playerMessage.text('');
  },

  events: {

    'NewDirection': function(dir) {
      // Don't change animation if we've just stopped moving
      // But do pause the current animation
      if (dir.x == 0 && dir.y == 0) {
        this.animationSpeed = 0;
        return;
      }
      else {
        this.animationSpeed = 1;
      }

      // Work out what animation to play
      var direction = (Math.abs(dir.y) > Math.abs(dir.x)) ?
                          (dir.y < 0 ? "up" : "down")     :
                          (dir.x < 0 ? "left" : "right")  ;
      var animation = "walk-" + direction;

      // Switch animation if it isn't already on
      if (this.spritesLoaded && this.reel() != animation)
        this.animate(animation, -1);
    },

    'NewMessage': function(data) {
      this.playerMessage.text(data.msg);
      this.playerMessage.textColor(data.priv ? 'purple' : 'white');
      this.cancelDelay(this.clearMessage);
      this.delay(this.clearMessage, 6000);
    },

    'RemotePlayerUpdate': function(data) {
      var keys = Object.keys(data.playerState);

      for (var i = 0; i < keys.length; ++i) {
        this.playerState[keys[i]] = data.playerState[keys[i]];
      }

      if (keys.indexOf('name') != -1) {
        this.playerName.text(data.playerState.name);
      }

      if (keys.indexOf('charset') != -1) {
        console.log('got new charset?');
        var filename = 'sprites/characters/' + charsets[data.playerState.charset] + '.png';
        this.charset(filename);
      }

      if (keys.indexOf('flair') != -1) {
        var flairText = data.playerState.flair.trim();
        this.playerFlair.text(flairText);
        if (flairText != '')
          this.playerMessage.y = this.y - 48;
        else
          this.playerMessage.y = this.y - 32;
      }
    }
  }
});

Crafty.c('RemoteCharacter', {
  required: 'Character',

  init: function () {
    this.lerpCoeff = 0;
    this.lerpTime = 0;
    this.oldPos = {x: this.x, y: this.y};
    this.newPos = {x: this.x, y: this.y};
    this.direction = {x: 0, y: 0};
  },

  events: {

    'RemotePlayerUpdate': function(data) {
      if (typeof data.playerState.pos != 'undefined') {
        this.lerpCoeff = 0;
        this.lerpTime = data.lerpTime;
        // Store the current pos to interpolate from
        this.oldPos.x = this.x;
        this.oldPos.y = this.y;
        // Store the new pos to interpolate to
        this.newPos.x = data.playerState.pos.x;
        this.newPos.y = data.playerState.pos.y;
      }

      if (typeof data.playerState.dimension != 'undefined') {
        this.dimension = data.playerState.dimension;
        this.trigger('UpdateVisibility', {});
      }
    },

    'UpdateVisibility': function() {
      // Show or hide character depending on what dimension they're in relative to local character
      var visible = this.dimension == Crafty('LocalCharacter').dimension;
      this.visible = visible;
      this.playerMessage.visible = visible;
      this.playerFlair.visible = visible;
      this.playerName.visible = visible;
    },

    'EnterFrame': function(data) {
      this.lerpCoeff = Crafty.math.clamp(this.lerpTime == 0 ?
        1 : this.lerpCoeff + data.dt / (1000.0 * this.lerpTime), 0, 1);
      var newX = (1-this.lerpCoeff) * this.oldPos.x + this.lerpCoeff * this.newPos.x;
      var newY = (1-this.lerpCoeff) * this.oldPos.y + this.lerpCoeff * this.newPos.y;

      var oldDirection = this.direction;
      this.direction = {x: Math.sign(newX - this.x), y: Math.sign(newY - this.y) };
      
      if (this.direction.x != oldDirection.x || this.direction.y != oldDirection.y) {
        this.trigger('NewDirection', this.direction);
      }

      this.x = newX;
      this.y = newY;
    }

  }
});

Crafty.c('LocalCharacter', {
  required: 'Character, Multiway, Collision',

  init: function() {
    this.multiway(0, {UP_ARROW: -90, DOWN_ARROW: 90, RIGHT_ARROW: 0, LEFT_ARROW: 180});

    this.bind("Moved", function() {
      if (typeof this.x != 'undefined') {
        var packet = { pos: {x: this.x, y: this.y} };
        socket.emit('player_motion', packet);
      }
    });

    // Initialise collision
    this.collision(new Crafty.polygon([6, 16, 26, 16, 26, 48, 6, 48]))

    // Create chunk highlight entity
    this.highlightEntity = Crafty.e('2D, Canvas, Color').attr({x:0,y:0,h:0,w:0,z:950}).color('rgba(0,0,0,0.2)');

    // Update current chunk and padlock
    this.updateCurrentChunk(false);
  },

  updateCurrentChunk: function(force) {
    // Check if our chunk's changed
    var chunkX = Math.floor(this._x / (chunkSize * 32));
    var chunkY = Math.floor(this._y / (chunkSize * 32));

    if ($('#lock').attr('mouseover')) {
      this.highlightEntity.attr({x:chunkX*chunkSize*32,y:chunkY*chunkSize*32,w:chunkSize*32,h:chunkSize*32});
    }
    else {
      this.highlightEntity.attr({w:0,h:0});
    }

    if (typeof this.chunkX === 'undefined' || this.chunkX != chunkX || this.chunkY != chunkY || force) {
      this.chunkX = chunkX;
      this.chunkY = chunkY;
      
      socket.emit('get_chunk_owner', {chunkIdx: [chunkX, chunkY, this.dimension]});
    }
  },

  tileBounds: function() {
    var box = this.cbr();

    // Correct cbr so we only take up 1 block
    // only the bottom tile of the character should collide
    box._y += 16;
    box._h -= 16;

    // also add some extra smallness to the player
    // to make the collision seem a little tighter
    box._x += 6;
    box._w -= 12;
    
    return box;
  },

  events: {

    'RemotePlayerUpdate': function(data) {
      var keys = Object.keys(data.playerState);

      if (keys.indexOf('speed') != -1) {
        this.speed(data.playerState.speed);
      }

      if (keys.indexOf('pos') != -1) {
        if (data.forceStates['pos']) {
          this.x = data.playerState.pos.x;
          this.y = data.playerState.pos.y;
        }
      }

      if (keys.indexOf('dimension') != -1) {
        console.log('got new dimension');
        this.dimension = data.playerState.dimension;
        Crafty('Terrain').trigger('DimensionChanged', {dimension:this.dimension});
        Crafty('RemoteCharacter').trigger('UpdateVisibility', {});
      }
    },

    'EnterFrame': function(data) {
      var vxc = -Crafty.viewport._x + 0.5 * Crafty.viewport._width / Crafty.viewport._scale;
      var vyc = -Crafty.viewport._y + 0.5 * Crafty.viewport._height / Crafty.viewport._scale;

      var diffx = Math.abs(vxc - this.x);
      var diffy = Math.abs(vyc - this.y);

      Crafty.viewport.x = (Crafty.viewport._width/Crafty.viewport._scale*0.5)-this.x;
      Crafty.viewport.y = (Crafty.viewport._height/Crafty.viewport._scale*0.4)-this.y;
    },

    'Moved': function(data) {
      Crafty('Terrain').trigger('ResolveCollisions', {entity: this, axis: data.axis.toLowerCase()});
      this.updateCurrentChunk(false);
    },

    'NewChunkData': function(data) {
      this.updateCurrentChunk(true);
    },

    'GetChunkOwner': function(data) {
      if (this.chunkX == data.chunkIdx[0] && this.chunkY == data.chunkIdx[1]) {
        $('#lock').attr('data-chunkidx', JSON.stringify(data.chunkIdx));
        if (data.owner != null) {
          $('#lock').attr('class', 'locked');
          $('#locked_name').text('Chunk owner: ' + data.owner);
        }
        else {
          $('#lock').attr('class', 'unlocked');
          $('#locked_name').text('Chunk owner: none');
        }
      }
    }
  }
});
