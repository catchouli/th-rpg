'use strict';

const chunkSize = 8;
const terrain_debug = false;

Crafty.c('SolidLayer', {
  required: "Renderable",
  init: function() {
    this.visible = (Crafty('Editor').currentLayer == SOLID_LAYER);
  },
  events: {
    'SetVisible': function(b) {
      this.visible = b;
    }
  }
});

Crafty.c('Terrain', {

  init: function() {
    this.viewportX = -Crafty.viewport._x;
    this.viewportY = -Crafty.viewport._y;

    this.chunkData = {};
    this.chunkEntities = {};
    this.chunksMonitoring = [];

    this.tilesetImages = {};
    this.dimension = "default";
  },

  updateVisibleChunks: function() {
    if (terrain_debug)
      console.log('updating visible chunks');
    
    var minX = -Crafty.viewport._x;
    var minY = -Crafty.viewport._y;
    var maxX = -Crafty.viewport._x + (Crafty.viewport._width / Crafty.viewport._scale);
    var maxY = -Crafty.viewport._y + (Crafty.viewport._height / Crafty.viewport._scale);

    var minChunkX = Math.floor(minX / 32 / chunkSize);
    var maxChunkX = Math.floor(maxX / 32 / chunkSize);
    var minChunkY = Math.floor(minY / 32 / chunkSize);
    var maxChunkY = Math.floor(maxY / 32 / chunkSize);

    var viewportWidth = Crafty.viewport._width / Crafty.viewport._scale;
    var viewportHeight = Crafty.viewport._height / Crafty.viewport._scale;

    // choose number of chunks to keep loaded based on viewport size
    const chunkCountX = viewportWidth / 32 / chunkSize * 1.0;
    const chunkCountY = viewportHeight / 32 / chunkSize * 1.0;

    // Get current centre chunk
    var chunkX = Math.floor(this.viewportX / (chunkSize*32));
    var chunkY = Math.floor(this.viewportY / (chunkSize*32));

    // Chunks we wanna load
    var newChunksMonitoring = [];

    for (var y = minChunkY; y <= maxChunkY; ++y) {
      for (var x = minChunkX; x <= maxChunkX; ++x) {
        var chunkIdx = [x, y, this.dimension];
        newChunksMonitoring.push(JSON.stringify(chunkIdx));
      }
    }

    // Chunks added or removed to rendering
    var newChunks = [];
    var removedChunks = [];

    // Check which chunks should be removed
    for (var i = 0; i < this.chunksMonitoring.length; ++i) {
      if (newChunksMonitoring.indexOf(this.chunksMonitoring[i]) == -1) {
        removedChunks.push(this.chunksMonitoring[i]);
        if (terrain_debug)
          console.log('removing chunk ' + this.chunksMonitoring[i]);
      }
    }

    // Check which chunks should be added
    for (var i = 0; i < newChunksMonitoring.length; ++i) {
      if (this.chunksMonitoring.indexOf(newChunksMonitoring[i]) == -1) {
        newChunks.push(newChunksMonitoring[i]);
        if (terrain_debug)
          console.log('adding chunk ' + newChunksMonitoring[i]);
      }
    }

    // Unload old chunks
    var chunkEntities, e;
    for (var i = 0; i < removedChunks.length; ++i) {
      var chunkIdx = removedChunks[i];
      if (terrain_debug)
        console.log('deleting chunk entity ' + chunkIdx);
      if (chunkEntities = this.chunkEntities[chunkIdx]) {
        for (var layer = BACKGROUND_LAYER; layer <= SOLID_LAYER; ++layer)
          chunkEntities[layer].destroy();
      }
      delete this.chunkData[chunkIdx];
      delete this.chunkEntities[chunkIdx];
    }

    // Tell server we no longer need the old chunks
    if (removedChunks.length > 0) {
      var stopMonitoringChunks = [];
      for (var i = 0; i < removedChunks.length; ++i)
        stopMonitoringChunks.push(JSON.parse(removedChunks[i]));

      socket.emit('stop_monitoring_chunks', {chunks: stopMonitoringChunks});
      if (terrain_debug) {
        console.log('stopping to monitor chunks:');
        console.log(removedChunks);
      }
    }

    // Ask server to monitor the new chunks for us
    if (newChunks.length > 0) {
      if (terrain_debug) {
        console.log('starting to monitor chunks:');
        console.log(newChunks);
      }

      var startMonitoringChunks = [];
      for (var i = 0; i < newChunks.length; ++i)
        startMonitoringChunks.push(JSON.parse(newChunks[i]));

      socket.emit('start_monitoring_chunks', {chunks: startMonitoringChunks});
    }

    // Update the chunks which are being monitored
    this.chunksMonitoring = newChunksMonitoring;
  },

  getTileData: function(x, y, layer) {
    var chunk = {x: Math.floor(x / chunkSize), y: Math.floor(y / chunkSize)};

    var chunkIdx = JSON.stringify([chunk.x,chunk.y,this.dimension]);
    if (this.chunkData[chunkIdx]) {
      var chunkTile = {x: ((x % chunkSize + chunkSize) % chunkSize),
                       y: ((y % chunkSize + chunkSize) % chunkSize)};
      var tile = this.chunkData[chunkIdx].blocks[layer][chunkTile.y * chunkSize + chunkTile.x];
      return tile;
    }

    return null;
  },

  getTileset: function(tileset, layer) {
    var type = (layer == SOLID_LAYER ? "collision" : "normal");
    var tilesets_arr = (type == "normal" ? tilesets : collision_tilesets);
    var tileset_src = (tileset < tilesets_arr.length ? tilesets_arr[tileset] : tilesets_arr[0]);

    if (tileset > tilesets_arr.length) {
      console.error("tileset " + tileset + " > than max of " + tilesets_arr.length);
    }
    
    if (!this.tilesetImages[type] || !this.tilesetImages[type][tileset]) {
      if (!this.tilesetImages[type])
        this.tilesetImages[type] = {};
      this.tilesetImages[type][tileset] = new Image();

      this.tilesetImages[type][tileset].onload = function() {
        Crafty('Terrain').trigger('RefreshAll', {});
      };

      this.tilesetImages[type][tileset].src = 'sprites/tilesets/' + tileset_src + '.png';
    }

    return this.tilesetImages[type][tileset];
  },

  events: {

    'ViewportScroll': function() {
      var viewportCentreX = -Crafty.viewport._x + (Crafty.viewport._width/Crafty.viewport._scale/2);
      var viewportCentreY = -Crafty.viewport._y + (Crafty.viewport._height/Crafty.viewport._scale/2);
      if (this.viewportX != viewportCentreX ||
          this.viewportY != viewportCentreY)
      {
        this.viewportX = viewportCentreX;
        this.viewportY = viewportCentreY;
        this.updateVisibleChunks();
      }
    },

    'ViewportResize': function() {
      var viewportCentreX = -Crafty.viewport._x + (Crafty.viewport._width/Crafty.viewport._scale/2);
      var viewportCentreY = -Crafty.viewport._y + (Crafty.viewport._height/Crafty.viewport._scale/2);
      this.viewportX = viewportCentreX;
      this.viewportY = viewportCentreY;
      this.updateVisibleChunks();
    },

    'NewChunkData': function(data) {
      var chunkIdx = JSON.stringify(data.id);

      // If we're no longer monitoring this chunk, don't create its entity
      // or render it
      // This can happen if a chunk is added and then removed before we get
      // any data back for it, and then this gets called but it's no longer
      // being tracked
      if (this.chunksMonitoring.indexOf(chunkIdx) == -1) {
        return;
      }

      var chunkX = data.id[0];
      var chunkY = data.id[1];

      var oldChunkData = this.chunkData[chunkIdx];
      var updateAll = data.updateAll || (typeof oldChunkData === 'undefined' ? true : false);

      if (typeof this.chunkData[chunkIdx] === 'undefined') {
        //this.chunkData[chunkIdx] = new Array(chunkSize * chunkSize).fill({});
        this.chunkData[chunkIdx] = data;
        this.chunkEntities[chunkIdx] = {};
        for (var layer = BACKGROUND_LAYER; layer <= SOLID_LAYER; ++layer) {
          // position layers around the player at z=100
          var z;
          if (layer == 0)
            z = 80;
          else if (layer == 1)
            z = 90;
          else
            z = 110;
          
          this.chunkEntities[chunkIdx][layer] = Crafty.e('2D, CachedCanvas, Color')
            .attr({x:chunkX*chunkSize*32, y:chunkY*chunkSize*32, w: chunkSize*32, h: chunkSize*32, z: z});
          this.chunkEntities[chunkIdx][layer].offscreenCanvas.width = chunkSize*32;
          this.chunkEntities[chunkIdx][layer].offscreenCanvas.height = chunkSize*32;

          if (layer == SOLID_LAYER)
            this.chunkEntities[chunkIdx][layer].addComponent('SolidLayer');
        }
        oldChunkData = this.chunkData[chunkIdx];
      }

      var chunkEntity = this.chunkEntities[chunkIdx];

      this.chunkData[chunkIdx] = data;

      for (var layer = BACKGROUND_LAYER; layer <= SOLID_LAYER; ++layer) {
        for (var y = 0; y < chunkSize; ++y) {
          for (var x = 0; x < chunkSize; ++x) {
            var idx = y * chunkSize + x;

            if (updateAll || !compareBlocks(oldChunkData.blocks[layer][idx], data.blocks[layer][idx]))
            {
              var defaultTilesetImage = this.getTileset(0, "normal");

              // Draw default grass below each background tile
              if (layer == 0)
                chunkEntity[layer].offscreenContext.drawImage(defaultTilesetImage, 0, 0, 32, 32, x*32, y*32, 32, 32);
              // For other layers, just clear them
              else
                chunkEntity[layer].offscreenContext.clearRect(x*32, y*32, 32, 32);

              // Draw the layer's tile
              if (typeof data.blocks[layer][idx].tileset !== 'undefined' && typeof data.blocks[layer][idx].tile !== 'undefined') {
                var tileset = data.blocks[layer][idx].tileset;
                var tilesetImage = this.getTileset(tileset, layer);

                // Draw tile
                if (data.blocks[layer][idx].tile != null) {
                  var srcTileX = data.blocks[layer][idx].tile % 8;
                  var srcTileY = Math.floor(data.blocks[layer][idx].tile / 8);

                  chunkEntity[layer].offscreenContext.drawImage(tilesetImage, srcTileX*32, srcTileY*32, 32, 32, x*32, y*32, 32, 32);
                }
              }
            }
          }
        }
      }
    },

    'ClearChunkData': function() {
      // Unload old chunks
      var chunkEntities, e;
      for (var i = 0; i < this.chunksMonitoring.length; ++i) {
        var chunkIdx = this.chunksMonitoring[i];
          console.log('deleting chunk entity ' + chunkIdx);
        if (chunkEntities = this.chunkEntities[chunkIdx]) {
          for (var layer = BACKGROUND_LAYER; layer <= SOLID_LAYER; ++layer)
            chunkEntities[layer].destroy();
        }
        delete this.chunkData[chunkIdx];
        delete this.chunkEntities[chunkIdx];
      }
    },

    'RefreshChunk': function(data) {
      var id = data.id;
      var chunkIdx = JSON.stringify(id);
      var oldChunkData = this.chunkData[chunkIdx];
      this.trigger('NewChunkData', {id: id, blocks: oldChunkData.blocks, updateAll: true});
    },

    'RefreshAll': function() {
      var keys = Object.keys(this.chunkData);
      for (var i = 0; i < keys.length; ++i) {
        this.trigger('RefreshChunk', {id: JSON.parse(keys[i])});
      }
    },

    'ResolveCollisions': function(data) {
      var ent = data.entity;
      var axis = data.axis;
      var bounds = ent.tileBounds();
      var entBox = {pos: { x: bounds._x + 0.5 * bounds._w, y: bounds._y + 0.5 * bounds._h },
                    half: { x: 0.5 * bounds._w, y: 0.5 * bounds._h}};

      var entTile = {x: Math.floor(entBox.pos.x / 32), y: Math.floor(entBox.pos.y / 32)};

      // Work out how many tiles to check based on the entities bounds
      var tileStartX = Math.floor(bounds._x / 32);
      var tileEndX = Math.floor((bounds._x + bounds._w) / 32);
      var tileStartY = Math.floor(bounds._y / 32);
      var tileEndY = Math.floor((bounds._y + bounds._h) / 32);
      var tilesToCheck = (tileEndX - tileStartX + 1) * (tileEndY - tileStartY + 1);

      // Debug code
      //// Make sure we've got enough debug entities
      //if (!this.collisionDebug)
      //  this.collisionDebug = [];
      //while (this.collisionDebug.length < tilesToCheck)
      //  this.collisionDebug.push(Crafty.e('2D, Canvas, Color').attr({x:0,y:0,w:0,h:0,z:10000}).color('red'));
      //while (this.collisionDebug.length > tilesToCheck) {
      //  this.collisionDebug[this.collisionDebug.length-1].destroy();
      //  this.collisionDebug.pop();
      //}

      // Consider only the region around the entity
      var idx = 0;
      var count = 0;
      for (var y = tileStartY; y <= tileEndY; ++y) {
        for (var x = tileStartX; x <= tileEndX; ++x) {
          //var debugEntity = this.collisionDebug[idx++];

          // recalculate entBox for each tile in case
          // resolving a collision has changed it
          var bounds = ent.tileBounds();
          var entBox = {pos: { x: bounds._x + 0.5 * bounds._w, y: bounds._y + 0.5 * bounds._h },
                        half: { x: 0.5 * bounds._w, y: 0.5 * bounds._h}};

          // get tile data
          var tile = this.getTileData(x, y, SOLID_LAYER);
          if (tile && tile.tile != null) {
            // tile bounds
            var tileBounds = {x: x*32, y: y*32, w: 32, h: 32};
            //debugEntity.attr(tileBounds);

            var tileBox = {pos: { x: tileBounds.x + 0.5 * tileBounds.w, y: tileBounds.y + 0.5 * tileBounds.h },
                           half: { x: 0.5 * tileBounds.w, y: 0.5 * tileBounds.h}};

            // check for intersection with tile
            var isect = intersectAABB(entBox, tileBox);
            if (isect) {
              if (isect.normal[axis] != 0) {
                ent[axis] += isect.delta[axis];
              }
              count++;
            }
          }
        }
      }
    },

    'DimensionChanged': function(data) {
      this.dimension = data.dimension;
      this.updateVisibleChunks();
    }
  }

});

// check if the two aabbs intersect and resolve it
function intersectAABB(a, b) {
  var dx = a.pos.x - b.pos.x;
  var px = (a.half.x + b.half.x) - Math.abs(dx);
  if (px < 0.0)
    return null;

  var dy = a.pos.y - b.pos.y;
  var py = (a.half.y + b.half.y) - Math.abs(dy);
  if (py < 0.0)
    return null;

  var hit = {pos: {x: 0, y: 0}, delta: {x: 0, y: 0}, normal: {x: 0, y: 0}};
  if (px < py) {
    var sx = Math.sign(dx);
    hit.delta = {x: px * sx, y: 0};
    hit.normal = {x: sx, y: 0};
    hit.pos.x = b.pos.x + (b.half.x * sx);
    hit.pos.y = a.pos.y;
  }
  else {
    var sy = Math.sign(dy);
    hit.delta = {y: py * sy, x: 0};
    hit.normal = {y: sy, x: 0};
    hit.pos.y = b.pos.y + (b.half.y * sy);
    hit.pos.x = a.pos.x;
  }
  return hit;
}


function compareBlocks(a, b) {
  if (typeof a === 'undefined' && typeof b !== 'undefined')
    return false;
  else if (typeof b === 'undefined' && typeof a !== 'undefined')
    return false;
  else
    return a.tileset == b.tileset && a.tile == b.tile && a.solid == b.solid;
}
