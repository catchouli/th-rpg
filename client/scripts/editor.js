'use strict';

Crafty.c('Editor', {
  required: '2D, Canvas, Mouse, Keyboard',

  init: function() {
    this.attr({w: Crafty.viewport._width / Crafty.viewport._scale, h: Crafty.viewport._height / Crafty.viewport._scale});

    this.mouseX = 0;
    this.mouseY = 0;
    this.mouseTileX = 0;
    this.mouseTileX = 0;
    this.mouseOver = false;
    this.mouseDown = [false, false, false];

    this.viewportX = Crafty.viewport.x;
    this.viewportY = Crafty.viewport.y;

    this.placing = Crafty.e('2D, Canvas, Color').attr({w: 32, h: 32, z: 150}).color('rgba(0,0,0,0.1)');
    this.placing.visible = false;

    this.currentLayer = -1;
  },

  updateMouseoverTile: function() {
    if (this.mouseOver) {
      this.mouseTileX = Math.floor(this.mouseX / 32);
      this.mouseTileY = Math.floor(this.mouseY / 32);
      this.placing.x = 32 * this.mouseTileX;
      this.placing.y = 32 * this.mouseTileY;
    }
  },

  placeBlock: function(tileX, tileY, invert) {
    if (this.currentLayer < 0 || this.currentLayer > SOLID_LAYER)
      return;

    var layer = this.currentLayer;
    var type = (layer == SOLID_LAYER ? "collision" : "normal");
    var tileset_selector = (type == "normal" ? "#tileset-select" : "#collision-tileset-select");
    var tileset = $(tileset_selector)[0].selectedIndex;
    var tile = parseInt($('#selected-block-'+type).attr('data-idx'));

    socket.emit('place_block', {tileX: tileX, tileY: tileY, tileset: tileset, tile: tile, layer: layer});
  },

  clearBlock: function(tileX, tileY, clearAll) {
    if (this.currentLayer < BACKGROUND_LAYER || this.currentLayer > SOLID_LAYER)
      return;
    
    // don't allow clearall on the solid layer
    var layer = (clearAll && this.currentLayer < SOLID_LAYER ? 'all' : this.currentLayer);

    console.log('clearing block from layer ' + layer);
    
    socket.emit('clear_block', {tileX: tileX, tileY: tileY, layer: layer});
  },

  placeBlockAtMouse: function(invert) {
    var tileX = Math.floor(this.mouseX / 32);
    var tileY = Math.floor(this.mouseY / 32);
    this.placeBlock(tileX, tileY, invert);
  },

  clearBlockAtMouse: function(clearAll) {
    var tileX = Math.floor(this.mouseX / 32);
    var tileY = Math.floor(this.mouseY / 32);
    this.clearBlock(tileX, tileY, clearAll);
  },

  sampleBlockAtMouse: function() {
    var layer = $('#placement-layer')[0].selectedIndex;

    var tileX = Math.floor(this.mouseX / 32);
    var tileY = Math.floor(this.mouseY / 32);

    var tileData = Crafty('Terrain').getTileData(tileX, tileY, layer);

    // sometimes it can be null, dunno why, maybe old data
    if (tileData.tile == null)
      tileData.tile = 0;
    if (tileData.tileset == null)
      tileData.tileset = 0;

    // Select this tileset and tile
    $('#tileset-select')[0].selectedIndex = tileData.tileset;
    selectTileset(tileData.tileset, function() {
      $('.block-select').each(function(i, e) {
        var idx = $(e).attr('data-idx');
        if (idx == tileData.tile) {
          $('#selected-block').removeAttr('id');
          $(e).attr('id', 'selected-block');
        }
      });
    });
  },

  placeOrClearBlockAtMouse: function() {
    if (this.isDown(Crafty.keys.SHIFT)) {
      if (this.mouseDown[Crafty.mouseButtons.LEFT])
        this.sampleBlockAtMouse();
    }
    else {
      if (this.mouseDown[Crafty.mouseButtons.LEFT])
        this.placeBlockAtMouse(this.isDown(Crafty.keys.CTRL));
      else if (this.mouseDown[Crafty.mouseButtons.RIGHT])
        this.clearBlockAtMouse(!this.isDown(Crafty.keys.CTRL));
    }
  },

  events: {

    'ViewportScroll': function() {
      this.x = 0 - Crafty.viewport._x;
      this.y = 0 - Crafty.viewport._y;
      this.mouseX += this.viewportX - Crafty.viewport.x;
      this.mouseY += this.viewportY - Crafty.viewport.y;
      this.viewportX = Crafty.viewport.x;
      this.viewportY = Crafty.viewport.y;
      this.updateMouseoverTile();
    },

    'ViewportResize': function() {
      this.attr({w: Crafty.viewport._width / Crafty.viewport._scale, h: Crafty.viewport._height / Crafty.viewport._scale});
    },

    'MouseOver': function(data) {
      this.mouseOver = true;
      this.mouseX = data.realX;
      this.mouseY = data.realY;
      this.placing.visible = true;
    },

    'MouseOut': function(data) {
      this.mouseOver = false;
      this.placing.visible = false;
    },

    'MouseMove': function(data) {
      this.mouseX = data.realX;
      this.mouseY = data.realY;
      var mouseTileX = this.mouseTileX;
      var mouseTileY = this.mouseTileY;
      this.updateMouseoverTile();
      if (mouseTileX != this.mouseTileX || mouseTileY != this.mouseTileY) {
        this.placeOrClearBlockAtMouse();
      }
    },

    'MouseDown': function(data) {
      this.mouseX = data.realX;
      this.mouseY = data.realY;
      this.mouseDown[data.mouseButton] = true;
      this.updateMouseoverTile();
      this.placeOrClearBlockAtMouse();
    },

    'MouseUp': function(data) {
      this.mouseDown[data.mouseButton] = false;
    },

    'ToolChanged': function(toolid) {
      if (toolid == 1)
        this.currentLayer = BACKGROUND_LAYER;
      else if (toolid == 2)
        this.currentLayer = MIDGROUND_LAYER;
      else if (toolid == 3)
        this.currentLayer = FOREGROUND_LAYER;
      else if (toolid == 4)
        this.currentLayer = OVERLAY_LAYER;
      else if (toolid == 5)
        this.currentLayer = SOLID_LAYER;
      else
        this.currentLayer = -1;

      if (this.currentLayer == SOLID_LAYER) {
        Crafty('SolidLayer').trigger('SetVisible', true);
      }
      else {
        Crafty('SolidLayer').trigger('SetVisible', false);
      }
    },

    'KeyDown': function(e) {
      if (e.key == Crafty.keys[1])
        $('#tools-buttons-select').click();
      if (e.key == Crafty.keys[2])
        $('#tools-buttons-background').click();
      if (e.key == Crafty.keys[3])
        $('#tools-buttons-midground').click();
      if (e.key == Crafty.keys[4])
        $('#tools-buttons-foreground').click();
      if (e.key == Crafty.keys[5])
        $('#tools-buttons-overlay').click();
      if (e.key == Crafty.keys[6])
        $('#tools-buttons-collision').click();
      if (e.key == Crafty.keys[7])
        $('#tools-buttons-object').click();
    }

  }
});
