'use strict';

// Submit chat messages when we press enter
$(function () {
  $('form').submit(function(){
    socket.emit('chat message', $('#m').val());
    $('#m').val('');
    $('#m').blur();
    scrollChatToBottom();
    return false;
  });
});

// Add message to chat log
function addToChatLog(msg) {
  // add to chat log
  var log = $('#chatlog');
  var scrolledToBottom = (log.scrollTop() + log.innerHeight() == log[0].scrollHeight);
  $('#chatlog p').append($('<li>').text(msg));
  if (scrolledToBottom) {
    scrollChatToBottom();
  }
}

function scrollChatToBottom() {
  var log = $('#chatlog');
  log.scrollTop(log[0].scrollHeight - log.innerHeight());
}

// Stop chat from passing events to the canvas
$('#chat').keydown(function(e) { e.stopPropagation(); });
$('#chat').mousedown(function(e) { e.stopPropagation(); });
$('#chat').mousemove(function(e) { e.stopPropagation(); });
