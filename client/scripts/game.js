'use strict';

// Initialise crafty
Crafty.init(undefined, undefined, document.getElementById('game'));
Crafty.background('url(grasstile.png)');

Crafty.defineScene('Login', function() {console.log('starting login scene');});
Crafty.defineScene('Game', function() {console.log('starting game scene');});
Crafty.enterScene('Login');
