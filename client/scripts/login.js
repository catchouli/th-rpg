var loginTimeout;
var loggedIn = false;

$('#login button').click(function() {
  disableForm();
  var username = $('#login #username').val();
  var password = $('#login #password').val();
  requestLogin(username, password);
  loginTimeout = setTimeout(function() {
    loginError('Connection timed out.');
  }, 1500);
});

function disableForm() {
  $('#login input').prop('disabled', true);
  $('#login button').prop('disabled', true);
  $('#login #error').css('color', 'black');
  $('#login #error').text('Logging in...');
}

function resetLoginForm(clear) {
  $('#login input').prop('disabled', false);
  $('#login button').prop('disabled', false);
  if (clear) {
    $('#login input#password').val('');
  }
}

function loginError(err) {
  clearTimeout(loginTimeout);
  $('#login #error').css('color', 'red');
  $('#login #error').text(err);
  resetLoginForm(false);
}

// called when login succeeds
function sessionBegin() {
  // hide login form
  clearTimeout(loginTimeout);
  resetLoginForm(true);
  $('#logincontainer').css('display', 'none');
  $('#chatlogbg').css('display', 'initial');
  $('#chat').css('display', 'initial');
  $('#tools').css('display', 'initial');
  $('#lock').css('display', 'initial');
}

// called when session ends
function sessionEnd() {
  // show login form
  loggedIn = false;
  $('#logincontainer').css('display', 'flex');
  $('#chatlogbg').css('display', 'none');
  $('#chat').css('display', 'none');
  $('#tools').css('display', 'none');
  $('#lock').css('display', 'none');
  $('#lockedname').css('display', 'none');
}

// attempt login
function requestLogin(username, password) {
  loggedIn = true;
  console.log(username);
  socket.emit('request_login', {client_version: clientVersion, username: username, password: password});  
}

function requestLoginSid(username, sid) {
  loggedIn = true;
  socket.emit('request_login', {client_version: clientVersion, username: username, sid: sid});  
  disableForm();
}

// handle autologin attempts
window.addEventListener("message", handleAutoLogin, false);
function handleAutoLogin(data) {
  // Don't allow anonymous to log in, that shit
  if (data.data.user == 'Anonymous') {
    $('#login input').css('display', 'none');
    $('#login label').css('display', 'none');
    $('#login button').css('display', 'none');
    loginError('Please log in to talkhaus to continue');
  }
  else {
    if (data.data.type == 'login')
      requestLoginSid(data.data.user, data.data.sid);
  }
}
