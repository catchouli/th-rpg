#!/usr/bin/env node

'use strict';

const fs = require('fs');
const express = require('express');
const http = require('http');
const server = require('./server/server.js');
const players = require('./server/players.js');
const client = require('./client/scripts/version.js');
const config = require('./config.js');

// Load config
if (fs.existsSync('config.local.js')) {
  console.log('Loading custom config from config.local.js');
}

// Create express app
var app = express();

// Create https server
var webServer = http.createServer(app);

// Set up listen ports
webServer.listen(config.httpPort, function() {
  console.log('listening on *:' + config.httpPort);
});

// Set up socket.io
var io = require('socket.io').listen(webServer);

// Serve files
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/client/index.html');
});
app.use('/', express.static(__dirname + '/client'));

// Start game server
console.log('server starting, client version: ' + client.version);
server.startServer(io, client.version);

// Handle ctrl-c
process.on('SIGINT', function () {
  players.disconnectAll('Server is shutting down.');
  process.exit();
});

