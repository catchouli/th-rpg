'use strict';

var r = require('rethinkdb');

exports.database = 'dev';

// Connect to datbase
r.connect({host: 'localhost', port: 28015}, function(err, conn) {
  if (err) throw err;
  exports.conn = conn;
});

