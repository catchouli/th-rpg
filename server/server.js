'use strict';

const Promise = require('bluebird');
const r = require('rethinkdb');
const db = require('./db.js');
const players = require('./players.js');
const auth = require('./auth.js');
const config = require('../config.js');

// Track connections - this is at a lower level to any gameserver stuff, and is raw connections
let connections = {};

// Starts the server, listening on io for connections
exports.startServer = function(io, version) {
  // Handle new user connections
  io.on('connection', function(socket) {
    console.log('user ' + socket.id + ' connected');

    // Store connection
    connections[socket.id] = socket;

    // Handle login request
    socket.on('request_login', function(d) { requestLogin.call(this, d, version); });

    // Remove connection on disconnect
    socket.on('disconnect', function() {
      delete connections[socket.id];
    });
  });
}

// Handle login requests by authenticating them with the
// authentication server and then handing off to the players
// module to load the player's save and allow them to
// enter the world
function requestLogin(data, version) {
  console.log('player ' + this.id + ' requested login');

  // Attempt authentication
  var ip = this.request.connection.remoteAddress;
  var loginData = {client_version: data.client_version, username: data.username,
    ip: ip, socket: this};

  if (data.sid) {
    console.log('user authenticating with sid');
    loginData.sid = data.sid;
  }
  else {
    console.log('user authenticating with password');
    loginData.password = data.password;
  }

  auth.authenticateLoginAsync(loginData)
      .then(function(data) { return verifyClientVersion(version, data); })
      .then(players.tryJoinWorld)
      .then(loginSucceeded)
      .catch(function(error) { loginFailed(loginData, error); });
}

// Called when a login succeeds
function loginSucceeded(data) {
  data.socket.emit('login_success', {});
  console.log('login succeeded, user id: ' + data.userId + ', username: ' + data.username);
  return data;
}

// Called when a login fails
function loginFailed(data, error) {
  console.log('login failed for user ' + data.socket.id);

  var hasUserError = typeof error.user_error !== 'undefined';
  var userError = (hasUserError ? error.user_error : 'Unknown error.');

  if (!hasUserError) {
    console.error('Unexpected error when logging in user ' + data.socket.id + ':');
    console.error(error);
  }

  data.socket.emit('login_failed', {error: userError});
}

// Verify the client's version is the same as server's
function verifyClientVersion(serverVersion, loginData) {
  if (serverVersion != loginData.client_version) {
    throw {user_error: 'Your client is out of date. Try refreshing.'};
  }

  return loginData;
}

// Auto refresh client when exiting
// This way, you can use nodemon to iterate over the client code easily
if (config.sendRefreshOnExit) {
  // Handle sigint and tell clients to refresh
  process.once('SIGUSR2', function() {
    console.log('Exiting. Refreshing clients.');

    // Tell all clients to refresh
    let keys = Object.keys(connections);
    for (let i = 0; i < keys.length; ++i) {
      connections[keys[i]].emit('refresh');
    }

    // Rethrow this signal so the process can exit
    process.kill(process.pid, 'SIGUSR2');
  });
}
