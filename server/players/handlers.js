const isstring = require('isstring');
const isNumeric = require('isnumeric');
const r = require('rethinkdb');
const commands = require('./commands.js');
const players = require('../players.js');
const terrain = require('../terrain.js');
const resources = require('../../client/scripts/resources.js');

// Handle ping messages
exports.onPling = onPling;
function onPling(player) {
  // store unix time when we get a ping
  player.privateState.lastPingTime = Date.now();
}

// Handle chat messages
exports.onChatMessage = onChatMessage;
function onChatMessage(player, msg) {
  if (!isstring(msg) || msg.length == 0)
    return;

  // Parse as command
  if (msg[0] == '/') {
    commands.parseCommand(player, msg.substr(1));
  }
  else {
    var packet = {user_id: this.id, playerName: player.publicState.name, message: msg.substr(0, 120)};
    players.broadcast(players._players, 'player_message', packet);
    console.log('[chat] ' + player.publicState.name + ': ' + msg.substr(0, 120));
  }
};

// Handler for when we don't own a chunk we tried to edit
function chunkErr(player, err, ownerId) {
  if (err == 'err_chunkowner' || err == 'err_dimensionowner') {
    var type = (err == 'err_chunkowner' ? 'chunk' : 'dimension');
    players.getPlayer(r.row('save_code').eq(ownerId))
      .then(function (res) {
        players.broadcastServerMessage(player, (res != null ? res.publicState.name : 'unknown') + ' is the owner of this ' + type);
    });
  }
  else if (err == 'err_chunklimit') {
    players.broadcastServerMessage(player, 'You already own your maximum number of chunks: ' + player.publicState.chunkCount + ', talk to an admin to get this limit increased');
  }
};

// Handler for when the player tries to place a block
exports.onPlaceBlock = onPlaceBlock;
function onPlaceBlock(player, data) {
  if (!isNumeric(data.tileX) || !isNumeric(data.tileY) || !isNumeric(data.tileset) || !isNumeric(data.tile) || !isNumeric(data.layer))
    return;

  terrain.placeBlock(player, data.tileX, data.tileY, data.tileset, data.tile, data.layer, function(e,o) { chunkErr(player,e,o); });
}

// Handler for when the player tries to clear a block
exports.onClearBlock = onClearBlock;
function onClearBlock(player, data) {
  if (!isNumeric(data.tileX) || !isNumeric(data.tileY) || (data.layer != 'all' && !isNumeric(data.layer)))
    return;

  terrain.clearBlock(player, data.tileX, data.tileY, data.layer, function(e,o) { chunkErr(player,e,o); });
}

// Handler for when the player wants to start monitoring a chunk or cuhnks
exports.onStartMonitoringChunks = onStartMonitoringChunks;
function onStartMonitoringChunks(player, data) {
  for (var i = 0; i < data.chunks.length; ++i) {
    var chunkX = data.chunks[i][0];
    var chunkY = data.chunks[i][1];

    if (!isNumeric(chunkX) || !isNumeric(chunkY))
      continue;

    terrain.monitorChunk(player, chunkX, chunkY);
  }
}

// Handler for when the player wants to stop monitoring a chunk or chunks
exports.onStopMonitoringChunks = onStopMonitoringChunks;
function onStopMonitoringChunks(player, data) {
  for (var i = 0; i < data.chunks.length; ++i) {
    var chunkIdx = data.chunks[i];
    var chunkX = data.chunks[i][0];
    var chunkY = data.chunks[i][1];

    if (!isNumeric(chunkX) || !isNumeric(chunkY))
      continue;
    
    terrain.stopMonitoringChunk(player, chunkX, chunkY);
  }
}

// Handler for when the player moves
exports.onPlayerMotion = onPlayerMotion;
function onPlayerMotion(player, data) {
  if (!isNumeric(data.pos.x) || !isNumeric(data.pos.y))
    return;
  
  player.publicState.pos = { x: data.pos.x, y: data.pos.y };
  player.publicStateDirty['pos'] = true;
  player.privateState.lastMoveTime = Date.now();
}

// Handler for when the player's client needs to get the owner of a chunk to update the padlock
exports.onGetChunkOwner = onGetChunkOwner;
function onGetChunkOwner(player, data) {
  var chunkX = data.chunkIdx[0];
  var chunkY = data.chunkIdx[1];
  var dimension = data.chunkIdx[2];

  if (!isNumeric(chunkX) || !isNumeric(chunkY) || !isstring(dimension))
    return;
  
  terrain.getChunkOwner(parseInt(chunkX), parseInt(chunkY), player.publicState.dimension, function(owner) {
    if (owner == null) {
      players.broadcast(player, 'get_chunk_owner', {chunkIdx: data.chunkIdx, owner: null});
    }
    else {
      players.getPlayer(r.row('save_code').eq(owner))
        .then(function(res) {
          players.broadcast(player, 'get_chunk_owner', {chunkIdx: data.chunkIdx, owner: (res != null ? res.publicState.name : 'unknown')});
        });
    }
  });
};

// Handler for when the player tries to toggle ownership of a chunk
exports.onToggleChunkOwner = onToggleChunkOwner;
function onToggleChunkOwner(player, data) {
  var chunkX = Math.floor(player.publicState.pos.x / (terrain.chunkSize * 32));
  var chunkY = Math.floor(player.publicState.pos.y / (terrain.chunkSize * 32));
  
  if (!isNumeric(chunkX) || !isNumeric(chunkY))
    return;
  
  terrain.toggleChunkOwner(player, chunkX, chunkY, player.publicState.dimension, function(e,o) { chunkErr(player,e,o); });
};

// Allow a player to select a character
exports.onSelectCharset = onSelectCharset;
function onSelectCharset(player, data) {
  if (!isNumeric(data.id))
    return;

  var charset = parseInt(data.id);
  if (charset >= 0 && charset < resources.charsets.length) {
    player.publicState.charset = charset;
    player.publicStateDirty['charset'] = true;
  }
};
