const isNumeric = require('isnumeric');
const r = require('rethinkdb');
const db = require('../db.js');
const players = require('../players.js');
const terrain = require('../terrain.js');

// Parse a command
// null player means it's a console issued command
// (not implemented yet but todo: support null player here)
exports.parseCommand = function(player, commandString) {
  var admin = player.publicState.admin;

  var match = commandString.match('([^ ]+)');
  if (match == null)
    return;

  var firstWord = match[1];

  var noSuchCommand = function() {
    players.broadcastServerMessage(player, "Invalid command: " + firstWord);
  }

  // Parse commands
  if (admin) {
    if (match = commandString.match('setname ([^ ]+) ([^ ]+)')) {
      var oldName = match[1];
      var newName = match[2];

      var other = players.findByName(oldName);

      if (!other) {
        players.broadcastServerMessage(player, "setname: Player " + oldName + " not found");
      }
      else {
        players.broadcastServerMessage(player, "setname: Setting " + oldName + "'s name to " + newName);

        other.publicState.name = newName;
        other.publicStateDirty['name'] = true;
      }

      return;
    }

    if (match = commandString.match('setflair ([^ ]+) (.+)')) {
      var name = match[1];
      var other = players.findByName(name);

      if (!other) {
        players.broadcastServerMessage(player, 'setflair: Player ' + oldName + ' not found');
      }
      else {
        players.broadcastServerMessage(player, 'setflair: Setting ' + oldName + "'s flair to " + match[2]);

        other.publicState.flair = match[2];
        other.publicStateDirty['flair'] = true;
      }

      return;
    }

    if (match = commandString.match('tp ([^ ]+) ([^ ]+) ([^ ]+)')) {
      var name = match[1];
      var other = players.findByName(name);
      var newX = match[2];
      var newY = match[3];

      if (!other) {
        players.broadcastServerMessage(player, 'tp: Player ' + name + ' not found');
      }
      else if (!isNumeric(newX) || !isNumeric(newY)) {
        players.broadcastServerMessage(player, 'tp: Arguments must be numbers');
      }
      else {
        players.broadcastServerMessage(player, 'tp: Teleporting ' + name + ' to (' + newX + ', ' + newY + ')');
        other.publicState.pos.x = parseFloat(newX) * 32;
        other.publicState.pos.y = parseFloat(newY) * 32;
        other.privateState.lastPos = other.publicState.pos;
        // force change to client
        other.publicStateDirty['pos'] = 'force';
      }

      return;
    }

    if (match = commandString.match('tp ([^ ]+) ([^ ]+)')) {
      var name = match[1];
      var other = players.findByName(name);
      var name2 = match[2];
      var other2 = players.findByName(name2);

      if (!other)
        players.broadcastServerMessage(player, 'tp: Player ' + name + ' not found');
      else if (!other2)
        players.broadcastServerMessage(player, 'tp: Player ' + name2 + ' not found');
      else {
        var newX = other2.publicState.pos.x;
        var newY = other2.publicState.pos.y;
        players.broadcastServerMessage(player, 'tp: Teleporting ' + name + ' to ' + name2 + ' (' + newX + ', ' + newY + ')');
        other.publicState.pos.x = parseFloat(newX);
        other.publicState.pos.y = parseFloat(newY);
        other.privateState.lastPos = other.publicState.pos;
        other.publicStateDirty['pos'] = 'force';
      }

      return;
    }

    if (match = commandString.match('kick ([^ ]+) ?(.+)?')) {
      var name = match[1];
      var reason = (typeof match[2] === 'undefined' ? '' : match[2]);
      console.log(reason);
      var other = players.findByName(name);

      if (!other) {
        players.broadcastServerMessage(player, 'kick: Player ' + name + ' is not on the server');
      }
      else {
        players.kickPlayer(other, 'Kicked by admin' + (reason == '' ? '.' : ': ') + reason);
      }

      return;
    }

    if (match = commandString.match('ban ([^ ]+) ?(.+)?')) {
      var name = match[1];
      var reason = (typeof match[2] === 'undefined' ? '' : match[2]);

      var other = players.findByName(name);

      if (!other) {
        players.broadcastServerMessage(player, 'ban: Player ' + name + ' is not on the server');
      }
      else {
        other.publicState.banned = reason;
        players.kickPlayer(other, 'Kicked by admin' + (reason == '' ? '.' : ': ') + reason);
      }

      return;
    }

    if (match = commandString.match('setchunkowner ([^ ]+)')) {
      var name = match[1];

      players.getPlayer(r.row('publicState')('name').eq(name))
        .then(function(res) {
          if (res == null) {
            players.broadcastServerMessage(player, 'setchunkowner: No such player ' + name);
          }
          else {
            var chunkX = Math.floor(player.publicState.pos.x / (terrain.chunkSize*32));
            var chunkY = Math.floor(player.publicState.pos.y / (terrain.chunkSize*32));
            terrain.setChunkOwner(res, chunkX, chunkY, player.publicState.dimension);
            players.broadcastServerMessage(player, 'setchunkowner: Setting owner of chunk (' + chunkX + ', ' + chunkY + ') to ' + name);
          }
        });

      return;
    }

    if (match = commandString.match('unsetchunkowner')) {
      var chunkX = Math.floor(player.publicState.pos.x / (terrain.chunkSize*32));
      var chunkY = Math.floor(player.publicState.pos.y / (terrain.chunkSize*32));
      terrain.setChunkOwner(null, chunkX, chunkY, player.publicState.dimension);
      players.broadcastServerMessage(player, 'unsetchunkowner: Unsetting owner of chunk (' + chunkX + ', ' + chunkY + ')');

      return;
    }

    if (match = commandString.match('clearchunkowner ([^ ]+)')) {
      var name = match[1];

      players.getPlayer(r.row('publicState')('name').eq(name))
        .then(function(res) {
          if (res == null) {
            players.broadcastServerMessage(player, 'clearchunkowner: Player ' + name + ' was not found');
          }
          else {
            terrain.clearChunkOwner(res.save_code);
            players.broadcastServerMessage(player, 'clearchunkowner: Chunks owned by ' + name + ' have been reclaimed');
          }
        });
    }

    if (match = commandString.match('setspeed ([^ ]+) ([^ ]+)')) {
      var name = match[1];
      var other = players.findByName(name);
      var speed = match[2];

      if (!other) {
        players.broadcastServerMessage(player, 'setspeed: Player ' + name + ' not found');
      }
      else if (!isNumeric(speed)) {
        players.broadcastServerMessage(player, 'setspeed: Speed must be a numeric value');
      }
      else {
        players.broadcastServerMessage(player, 'setspeed: Setting ' + name + "'s speed to " + speed);
        other.publicState.speed = speed;
        other.publicStateDirty['speed'] = true;
      }

      return;
    }
  }

  if (match = commandString.match('msg ([^ ]+) (.+)')) {
    var name = match[1];
    var msg = match[2];
    var other = players.findByName(name);

    if (!other) {
      players.broadcastServerMessage(player, 'msg: No such player ' + name);
    }
    else {
      var packet = { out: true, playerName: player.publicState.name, message: msg, user_id: player.user_id };
      players.broadcast(player, 'private_message', packet);
      packet.out = false;
      players.broadcast(other, 'private_message', packet);
    }

    return;    
  }

  if (match = commandString.match('escape')) {
    players.broadcastServerMessage(player, 'escape: Teleporting player to 0, 0');
    players.escape(player);

    return;
  }

  if (match = commandString.match('(addfriend|removefriend) ([^ ]+)')) {
    var cmd = match[1];
    var name = match[2];

    players.getPlayer(r.row('publicState')('name').eq(name))
      .then(function(res) {
        if (res == null) {
          players.broadcastServerMessage(player, cmd + ': No such player ' + name + ' found');
        }
        else {
          var playerInList = player.publicState.friends.indexOf(res.save_code) != -1;
          
          if (cmd == 'addfriend' && playerInList) {
            players.broadcastServerMessage(player, cmd + ': ' + name + ' is already in your friends list');
          }
          else if (cmd == 'removefriend' && !playerInList) {
            players.broadcastServerMessage(player, cmd + ': ' + name + ' is not in your friends list');
          }
          else {
            var action = (cmd == 'addfriend' ? 'Adding' : 'Removing');
            var preposition = (cmd == 'addfriend' ? 'to' : 'from');
            var msg = cmd + ': ' + action + ' player ' + res.publicState.name + ' ' + preposition + ' friends list';
            players.broadcastServerMessage(player, msg);

            if (cmd == 'addfriend') {
              player.publicState.friends.push(res.save_code);
            }
            else {
              var fn = function(v) { return v != res.save_code; };
              player.publicState.friends = player.publicState.friends.filter(fn);
            }

            // Check if this player is online and notify them if so
            var other = players.findBySaveId(res.save_code);
            if (other && cmd == 'addfriend') {
              players.broadcastServerMessage(other, 'You have been added to ' + player.publicState.name + '\'s friends list');
            }

            // Save this player so their friendlist updates immediately
            players.savePlayer(player);
          }
        }
      });

    return;
  }

  if (match = commandString.match('friends')) {
    players.broadcastServerMessage(player, 'Players on your friends list:');
    for (var i = 0; i < player.publicState.friends.length; ++i) {
      players.getPlayer(r.row('save_code').eq(player.publicState.friends[i]))
        .then(function(res) {
          if (res != null)
            players.broadcastServerMessage(player, res.publicState.name);
        });
    }

    return;
  }

  if (match = commandString.match('getchunkowner')) {
    var chunkX = Math.floor(player.publicState.pos.x / (terrain.chunkSize*32));
    var chunkY = Math.floor(player.publicState.pos.y / (terrain.chunkSize*32));
    terrain.getChunkOwner(chunkX, chunkY, function(owner) {
      if (owner) {
        players.getPlayer(r.row('save_code').eq(owner))
          .then(function(res) {
            if (res == null) {
              players.broadcastServerMessage(player, 'getchunkowner: An invalid player owns the chunk');
            }
            else {
              players.broadcastServerMessage(player, 'getchunkowner: ' + res.publicState.name + ' owns the chunk');
            }
          });
      }
      else {
        players.broadcastServerMessage(player, 'getchunkowner: Chunk has no owner');
      }
    });
    
    return;
  }

  if (match = commandString.match('pos ([^ ]+)')) {
    var name = match[1];
    var other = players.findByName(name);

    if (!other)
      players.broadcastServerMessage(player, 'tp: Player ' + name + ' not found');
    else {
      var x = Math.round(other.publicState.pos.x / (32));
      var y = Math.round(other.publicState.pos.y / (32));
        players.broadcastServerMessage(player, 'Position of user ' + name + ': (' + x + ', ' + y + ')');
    }

    return;
  }

  if (match = commandString.match('pos')) {
    var x = Math.round(player.publicState.pos.x / (32));
    var y = Math.round(player.publicState.pos.y / (32));
    players.broadcastServerMessage(player, 'Position of user ' + player.publicState.name + ': (' + x + ', ' + y + ')');

    return;
  }

  if (match = commandString.match('changeflair (.+)')) {
    var flair = match[1].substr(0, 35);

    players.broadcastServerMessage(player, 'changeflair: Setting flair to ' + flair);

    player.publicState.flair = flair;
    player.publicStateDirty['flair'] = true;

    return;
  }

  if (match = commandString.match('tp ([^ ]+)')) {
    var name = match[1];
    var other = players.findByName(name);

    if (!other)
      players.broadcastServerMessage(player, 'tp: Player ' + name + ' not found');
    else {
      var newX = other.publicState.pos.x;
      var newY = other.publicState.pos.y;
      players.broadcastServerMessage(player, 'tp: Teleporting to ' + name);
      player.publicState.pos.x = parseFloat(newX);
      player.publicState.pos.y = parseFloat(newY);
      player.privateState.lastPos = other.publicState.pos;
      player.publicStateDirty['pos'] = 'force';
    }

    return;
  }

  if (match = commandString.match('players')) {
    var keys = Object.keys(players._players);
    players.broadcastServerMessage(player, 'Online players: (' + keys.length + ')');
    for (var i = 0; i < keys.length; ++i) {
      players.broadcastServerMessage(player, ' - ' + players._players[keys[i]].publicState.name);
    }

    return;
  }

  if (match = commandString.match('dimension ([^ ]+)')) {
    if (match[1] != '') {
      players.changeDimension(player, match[1]);
    }

    return;
  }

  if (match = commandString.match('dimension')) {
    players.broadcastServerMessage(player, 'You are in dimension ' + player.publicState.dimension);
    return;
  }


  noSuchCommand();
}

