const players = require('../players.js');

// Handle pinging out and inactivity timeout
exports.every30Seconds = every30Seconds;
function every30Seconds(player) {
  // If it's been more than 120 seconds since a ping ping out
  if (Date.now() - player.privateState.lastPingTime > 120000) {
    console.error('Ping timeout: ' + player.publicState.name);
    players.kickPlayer(player, 'Ping timeout');
  }

  // If it's been more than so many minutes since the last player movement
  // time them out for inactivity
  var activityTimeout = 30;
  var idleTime = Date.now() - player.privateState.lastMoveTime;
  if (idleTime > (activityTimeout * 60 * 1000)) {
    console.error('Activity ping out: ' + player.publicState.name);
    players.kickPlayer(player, 'You have been timed out due to inactivity');
  }
}

// Limits player's speed to their max speed
exports.limitPlayerSpeed = limitPlayerSpeed;
function limitPlayerSpeed(player) {
  var pos = {x: player.publicState.pos.x, y: player.publicState.pos.y };
  var vel = {x: pos.x - player.privateState.lastPos.x, y: pos.y - player.privateState.lastPos.y};
  var speed = Math.sqrt(vel.x * vel.x + vel.y * vel.y);

  // Max abs velocity is (player.speed, player.speed)
  // Tolerancce, the player can go this much faster before getting disconnected
  var tolerance = 0.15;
  var maxSpeed = (1.0 + tolerance) * Math.ceil(Math.sqrt(player.publicState.speed * player.publicState.speed * 2));

  if (speed > maxSpeed) {
    console.log('Player speed exceeded max speed! Moving player back.');

    var scale = maxSpeed / speed;
    player.publicState.pos.x = player.privateState.lastPos.x + vel.x * scale;
    player.publicState.pos.y = player.privateState.lastPos.y + vel.y * scale;
    player.publicStateDirty["pos"] = 'force';
    players.broadcastPlayerState(player, 0);

    players.kickPlayer(player, 'Exceeded max speed');
  }

  player.privateState.lastPos = pos;
};

