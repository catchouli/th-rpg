'use strict';

const Promise = require('bluebird');
const r = require('rethinkdb');
const clone = require('clone');
const isNumeric = require('isnumeric');
const isstring = require('isstring');
const isbool = require('lodash.isboolean');
const db = require('./db.js');
const terrain = require('./terrain.js');
const resources = require('../client/scripts/resources.js');
const handlers = require('./players/handlers.js');
const intervals = require('./players/intervals.js');
const commands = require('./players/commands.js');

var _players = [];
exports._players = _players;

// Main entry point for this module, tries to load the save for a player,
// or creates a new one if the save id does not exist. The username parameter
// specifies the default username for this player, obtained from the auth
// server. The username in the database overrides it, however.
exports.tryJoinWorld = function(data) {
  // Check this socket doesn't already have a connection
  if (findBySocket(data.socket) != null) {
    throw {user_error: 'Client is already logged in.'};
  }

  // Try loading the player and entering it into the world
  // Save id is the user's (auth server) user id
  var saveId = data.userId;
  loadPlayerBySaveCode(data.socket, data.username.replace(/ /g, '_'), true, saveId)
    .then(checkBanned)
    .then(enterWorld)
    .catch(function(e) {
      console.log('EnterWorld error: ' + e);
    });

  return data;
}

// Teleport a player back to the origin, accessible by /escape and some other conditions
exports.escape = escape;
function escape(player) {
  changeDimension(player, "default");
  player.publicState.pos.x = 0;
  player.publicState.pos.y = 0;
  player.privateState.lastPos = player.publicState.pos;
  player.publicStateDirty['pos'] = 'force';
}

// Teleport a player to another dimension
exports.changeDimension = changeDimension;
function changeDimension(player, dimension) {
  // Get dimension and check if it's new
  terrain.getDimension(dimension, true)
    .then(function(dim) {
      if (dim.isNew) {
        broadcastServerMessage(player, 'Creating new dimension ' + dimension);
        terrain.setDimensionOwner(dimension, player);
      }

      // Update player dimension
      broadcastServerMessage(player, 'Changing to dimension ' + dimension);
      player.publicState.dimension = dimension;
      player.publicStateDirty['dimension'] = true;
    });
}

// Check if a player is banned and then tell them they are and the reason and force disconnect them
function checkBanned(player) {
  if (typeof player.publicState.banned !== 'undefined') {
    var reason = player.publicState.banned;
    var msg = 'You are banned' + (reason == '' ? '.' : ': ') + reason;
    player.socket.emit('force_disconnect', {error: msg});
    throw 'User ' + player.name + ' is banned';
  }

  return player;
}

// Set up a player, adding all of its handlers and intervals
function enterWorld(player) {
  // Check this player isn't already logged in
  var existing = findBySaveId(player.save_code);
  if (existing) {
    kickPlayer(existing, 'You have logged into this character from another place');
  }

  // Add player
  // Doing this after notifying the player about all other players
  // so that loop doesn't inculde itself
  _players[player.socket.id] = player;

  // Notify everyone that this player has joined
  broadcastPlayerJoined(player);

  // Check for dirty player state and broadcast it every 50ms
  addPlayerInterval(player, 50.0, function(player) { broadcastPlayerState(player, 50.0 / 1000.0); });

  // Check player hasn't exceeded their max speed once a second
  player.privateState.lastPos = clone(player.publicState.pos);
  addPlayerInterval(player, 1000, intervals.limitPlayerSpeed);

  // Auto-save players every 10 seconds
  addPlayerInterval(player, 10000, savePlayer);

  // Handle pinging out and inactivity timeout
  addPlayerInterval(player, 2000, intervals.every30Seconds);

  // Handle ping
  addPlayerHandler(player, 'pling', handlers.onPling);

  // Handle disconnection
  addPlayerHandler(player, 'disconnect', leaveWorld);

  // Handle chat messages
  addPlayerHandler(player, 'chat message', handlers.onChatMessage);

  // Handle the player placing a block
  addPlayerHandler(player, 'place_block', handlers.onPlaceBlock);

  // Handle the player trying to clear a block
  addPlayerHandler(player, 'clear_block', handlers.onClearBlock);

  // Handle request to start monitoring a chunk of the map
  addPlayerHandler(player, 'start_monitoring_chunks', handlers.onStartMonitoringChunks);

  // Handle request to stop monitoring a chunk of the map
  addPlayerHandler(player, 'stop_monitoring_chunks', handlers.onStopMonitoringChunks);

  // Position updates
  addPlayerHandler(player, 'player_motion', handlers.onPlayerMotion);

  // Allow player to select charset
  addPlayerHandler(player, 'select_charset', handlers.onSelectCharset);

  // Allow player to request the owner of a chunk
  addPlayerHandler(player, 'get_chunk_owner', handlers.onGetChunkOwner);

  // Allow player to try and take ownership of a chunk
  addPlayerHandler(player, 'toggle_chunk_owner', handlers.onToggleChunkOwner);

  return player;
}

// Called to add an event handler to a player, making it exception safe
function addPlayerHandler(player, evt, cb) {
  if (typeof player.privateState.eventHandlers === 'undefined')
    player.privateState.eventHandlers = [];

  player.privateState.eventHandlers.push(evt);

  player.socket.on(evt, function(data) {
    try {
      cb.call(this, player, data);
    }
    catch (e) {
      onPlayerException(player, e); 
    }
  });
}

// Called to remove all player event handlers
function removePlayerHandlers(player) {
  if (typeof player.privateState.eventHandlers === 'undefined')
    player.privateState.eventHandlers = [];

  for (var i = 0; i < player.privateState.eventHandlers.length; ++i) {
    var evt = player.privateState.eventHandlers[i];
    player.socket.removeAllListeners(evt);
  }

  player.privateState.eventHandlers = [];
}

// Called to add an interval to a player, making it exception safe
function addPlayerInterval(player, ms, cb) {
  // Run a function on a user every interval until they leave the server
  // Returns the interval, so that it can be used to cancel this
  function playerInterval(player, ms, func) {
    var interval = setInterval(myFunc, ms);
    function myFunc() {
      func(player);
    }
    return interval;
  }

  if (typeof player.privateState.intervals === 'undefined')
    player.privateState.intervals = [];

  var interval = playerInterval(player, ms, function(player) {
    try {
      cb.call(this, player);
    }
    catch (e) {
      onPlayerException(player, e);
    }
  });

  player.privateState.intervals.push(interval);
}

// Called to remove all player intervals
function removePlayerIntervals(player) {
  if (typeof player.privateState.intervals === 'undefined')
    player.privateState.intervals = [];

  for (var i = 0; i < player.privateState.intervals.length; ++i) {
    var interval = player.privateState.intervals[i];
    clearInterval(interval);
  }

  player.privateState.intervals = [];
}

// Called when a player somehow causes an exception
function onPlayerException(player, e) {
  // Log error
  console.error('Player error (' + player.publicState.name + ')');
  console.error(e);

  // Kick player
  kickPlayer(player, 'Error in session.');
}

// Forcefully remove player from the server
exports.kickPlayer = kickPlayer;
function kickPlayer(player, msg) {
  // Save player
  savePlayer(player);

  // Remove player from the world
  leaveWorld(player);

  // Tell the player they've been disconnected
  player.socket.emit('force_disconnect', {error: msg});
}

// Disconnect all players form the server
exports.disconnectAll = function(msg) {
  var keys = Object.keys(_players);
  for (var i = 0; i < keys.length; ++i) {
    kickPlayer(_players[keys[i]], msg);
  }
}

// Called when a player leaves the world to clean up behind them
function leaveWorld(player) {
  console.log('user ' + player.user_id + ' disconnected');

  // Remove the player's events
  removePlayerHandlers(player);

  // Remove player intervals
  removePlayerIntervals(player);

  // Stop monitoring any chunks
  terrain.stopMonitoringAll(player);

  // Notify everyone that this player has left
  notifyPlayerLeft(player);

  // Remove the player
  delete _players[player.user_id];

  // Save the player's game
  savePlayer(player);
}

// Broadcast a packet to one or more players, optionally excepting players
exports.broadcast = broadcast;
function broadcast(players, type, packet, except) {
  if (typeof except === 'undefined')
    except = [];
  if (!Array.isArray(except)) {
    except = [except];
  }
  if (!Array.isArray(players))
    players = [players];

  var keys = Object.keys(players);
  for (var i = 0; i < keys.length; ++i) {
    var player = players[keys[i]];

    if (except.indexOf(player) == -1) {
      player.socket.emit(type, packet);
    }
  }
}

// Send a server message to a player or players
exports.broadcastServerMessage = broadcastServerMessage;
function broadcastServerMessage(players, message, except) {
  var packet = { message: message }
  broadcast(players, 'server_message', packet, except);
}

// A player joined, notify players
function broadcastPlayerJoined(player) {
  var packet = { playerState: player.publicState
    , user_id: player.user_id
    , lerpTime: 0
    , forceStates: {}
  }

  // Notify local player
  packet.local = true;
  broadcast(player, 'player_joined', packet);

  // Notify all other players
  packet.local = false;
  broadcast(_players, 'player_joined', packet, player);

  // Notify this player of all other players
  var keys = Object.keys(_players);
  for (var i = 0; i < keys.length; ++i) {
    var otherPlayer = _players[keys[i]];

    packet = { playerState: otherPlayer.publicState
      , user_id: otherPlayer.user_id
      , lerpTime: 0
    }

    if (otherPlayer != player)
      broadcast(player, 'player_joined', packet);
  }

  // Welcome the player
  broadcastServerMessage(player, 'Welcome to the world of worldhaus!');
  broadcastServerMessage(player, 'Current version: 1.5.1 (2017-06-21)');
  broadcastServerMessage(player, 'Feature update:');
  broadcastServerMessage(player, ' - Added a dimensions system, currently accessible via command: /dimension test to get to the dimension \'test\' or /dimension default to get back to the default');
  broadcastServerMessage(player, ' - Hotfix: fixed oversight in player visibility between dimensions, and make the /dimension command tell you what dimension you\'re currently in');
  broadcast(player, 'scroll_chat_to_bottom', {});
}

// Broadcast player state (e.g. movement)
// The second parameter is the amount of time to lerp this over
// 0 = immediate. If you're broadcasting the player state every
// 50ms for example, use 0.05
exports.broadcastPlayerState = broadcastPlayerState;
function broadcastPlayerState(player, lerpTime) {
  var dirtyKeys = Object.keys(player.publicStateDirty);
  if (dirtyKeys.length > 0) {
    var packet = {
      playerState: {},
      forceStates: {},
      user_id: player.user_id,
      lerpTime: lerpTime
    };

    for (var i = 0; i < dirtyKeys.length; ++i) {
      // val: usually true, but occasionally 'force' to signal
      // to the client that this change should be forced
      // even if it's to a local character
      var val = player.publicStateDirty[dirtyKeys[i]];
      packet.playerState[dirtyKeys[i]] = player.publicState[dirtyKeys[i]];
      if (val == 'force')
        packet.forceStates[dirtyKeys[i]] = true;
    }

    broadcast(_players, 'player_update', packet);
  }
  player.publicStateDirty = [];
}

// Notify a socket that a player has left
function notifyPlayerLeft(player) {
  var packet = {user_id: player.user_id};
  broadcast(_players, 'player_left', packet);
}

// Get characters that match a filter criteria
// See loadPlayerBySaveCode
exports.getPlayers = getPlayers;
function getPlayers(filterCriteria) {
  return r.db(db.database).table('characters')
    .filter(filterCriteria)
    .run(db.conn)
    .then(function(cursor) { return cursor.toArray(); });
}

// Get a single character that matches a filter critera
// Logs an error if there's more than 1, or returns null if there's 0
exports.getPlayer = getPlayer;
function getPlayer(filterCriteria) {
  return getPlayers(filterCriteria)
    .then(function(res) {
      if (res.length == 0)
        return null;
      else if (res.length == 1)
        return res[0];
      else {
        console.error(new Error('getPlayer returned multiple players').stack);
        return null;
      }
    });
}

// Load player from db or create new, the create parameter
// is whether to create the player and save it to the database,
// or just return null in the case of a nonexistent save code
function loadPlayerBySaveCode(socket, defaultName, create, save_code) {
  // Default player values
  var player = { user_id: socket.id
    , save_code: save_code
    , socket: socket
    , publicState:
    { pos: {x: 50, y: 50}
      , speed: 200
      , name: defaultName
      , flair: ''
      , admin: false
      , friends: []
      , charset: Math.floor(Math.random()*resources.charsets.length-1)
      , chunkCount: 0
      , chunkLimit: 200
      , dimension: "default"
    }
    , publicStateDirty: {}
    , privateState: 
    { lastPingTime: Date.now()
      , lastMoveTime: Date.now()
    }
  };

  // Attempt to load player info from db
  return getPlayer(r.row('save_code').eq(save_code))
    .then(function(result) {
      if (result != null) {
        console.log('save loaded');

        var publicState = result.publicState;
        var keys = Object.keys(publicState);
        for (var i = 0; i < keys.length; ++i) {
          player.publicState[keys[i]] = publicState[keys[i]];
        }

        // Hard-code the chunk limit to 200 since we don't have migrations yet
        // todo: make a migration to update <200 ones to 200
        if (player.publicState.chunkLimit < 200)
          player.publicState.chunkLimit = 200;
        player.publicState.speed = 200;

        return player;
      }
      else if (create) {
        console.log('saving new character');
        broadcastServerMessage(player, 'This appears to be your first time. Creating new character.');
        savePlayer(player);

        return player;
      }
      else {
        return null;
      }
    });
}

// Save player state to database
exports.savePlayer = savePlayer;
function savePlayer(player) {
  var saveData = { publicState: player.publicState
                 , save_code: player.save_code
                 };
  r.db(db.database).table('characters').
    insert([saveData], {conflict: 'update'}).
    run(db.conn, function(err, result) {
      if (err) throw err;
    });
}

// Find a player from a user id
function findById(id) {
  var player = _players[id];
  if (typeof player === 'undefined')
    return null;
  else
    return player;
}

// Find a player from a socket
function findBySocket(socket) {
  return findById(socket.id);
}

// Find a player by name
exports.findByName = findByName;
function findByName(name) {
  var keys = Object.keys(_players);
  for (var i = 0; i < keys.length; ++i) {
    if (_players[keys[i]].publicState.name == name) {
      return _players[keys[i]];
    }
  }

  return null;
}

// Find a player by save id
exports.findBySaveId = findBySaveId;
function findBySaveId(saveId) {
  var keys = Object.keys(_players);
  for (var i = 0; i < keys.length; ++i) {
    if (_players[keys[i]].save_code == saveId) {
      return _players[keys[i]];
    }
  }

  return null;
}
