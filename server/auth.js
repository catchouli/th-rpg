'use strict'

const Promise = require('bluebird');
const request = Promise.promisifyAll(require('request'));

// Authenticate login
exports.authenticateLoginAsync = function(data) {
  // Try authenticating and then return the result
  var authData = { form: { username: data.username, ip: data.ip } };

  if (data.sid)
    authData.form.sid = data.sid;
  else
    authData.form.password = data.password;

  return request.postAsync('https://talkhaus.raocow.com/auth.php', authData)
                .then(function(res, body) { return handleAuthResponse(res, body, data); });
}

// Handle a response from the auth server
function handleAuthResponse(response, body, data) {
  if (response.statusCode == 200) {
    var login_result = response.headers['login_result'];
    if (typeof login_result !== 'undefined') {
      if (login_result == 3) {
        var user_id = response.headers['user_id'];
        var user_name = response.headers['user_name'];
        if (typeof user_id !== 'undefined' && typeof user_name !== 'undefined') {
          return {userId: user_id, username: user_name, socket: data.socket, client_version: data.client_version};
        }
        else {
          throw {user_error: 'Failed to get user id or user name.'};
        }
      }
      else if (login_result == 13) {
        throw {user_error: 'Too many invalid login attempts.'};
      }
      else {
        throw {user_error: 'Failed to authenticate user.'};
      }
    }
    else {
      throw {user_error: 'Unknown login error.'};
    }
  }
}

