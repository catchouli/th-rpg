'use strict';

const r = require('rethinkdb');
const isNumeric = require('isnumeric');
const db = require('./db.js');
const players = require('./players.js');
const resources = require('../client/scripts/resources.js');
const layers = require('../client/scripts/layers.js');

// Chunk size
const chunkSize = 8;
exports.chunkSize = chunkSize;

// Place a block at a position
exports.placeBlock = function(player, x, y, tileset, tile, layer, cb) {
  var tilesets_arr = (layer == layers.SOLID_LAYER ? resources.collision_tilesets : resources.tilesets);
  
  if (tileset < 0 || tileset >= tilesets_arr.length) {
    console.error('got invalid tileset: ' + tileset);
    return;
  }

  if (layer < layers.BACKGROUND_LAYER || layer > layers.SOLID_LAYER) {
    console.error('got invalid layer: ' + layer);
    return;
  }

  changeBlock(player, x, y, layer, {tileset: tileset, tile: tile}, cb);
}

// Clear a block at a position
exports.clearBlock = function(player, x, y, layer, cb) {
  if (layer != 'all' && (layer < layers.BACKGROUND_LAYER || layer > layers.SOLID_LAYER)) {
    console.error('got invalid layer: ' + layer);
    return;
  }
  
  changeBlock(player, x, y, layer, {tileset: 0, tile: null}, cb);
}

// Start monitoring a chunk for a player and notifying them
// when it's updated
exports.monitorChunk = function(player, chunkX, chunkY) {
  let dimension = player.publicState.dimension;
  let chunkIdx = [chunkX, chunkY, dimension];

  if (!isNumeric(chunkX) || !isNumeric(chunkY)) {
    console.error('player requisted we monitor invalid chunk: >' + chunkX + '<, >' + chunkY + '<');
    return;
  }

  // Initialise player chunks being monitored collection
  if (typeof player.privateState.chunks === 'undefined')
    player.privateState.chunks = {};

  if (Object.keys(player.privateState.chunks).length > 400) {
    console.error('player ' + player.publicState.name + ' is monitoring too many chunks');
    return;
  }

  if (chunkX < -100000 || chunkX > 100000 || chunkY < -100000 || chunkY > 100000) {
    console.error('player ' + player.publicState.name + ' requested out of bounds chunk ' + chunkX + ',' + chunkY);
    escape(player);
  }

  // Send initial chunk data
  getChunk(chunkX, chunkY, dimension)
    .then(function(chunk) { sendChunk(chunk, player); })
    .catch(function(err) {
      console.error(err);  
    });

  // Add changefeed for chunk for player
  r.db(db.database).table('map').get(chunkIdx).changes()
    .run(db.conn, function(err, cursor) {
      if (err) throw err;

      if (typeof player.privateState.chunks[chunkIdx] !== 'undefined') {
        console.error('tried to add a duplicate watch for player ' + player.publicState.name);
        cursor.close();
      }
      else {
        // Store changefeed cursor so we can remove it
        player.privateState.chunks[chunkIdx] = cursor;

        // Dispatch chunk updates to player
        cursor.each(function(err, data) {
          if (err) throw err;

          sendChunk(data.new_val, player);
        });
      }
    });
}

// Stop monitoring a chunk for a player
exports.stopMonitoringChunk = function(player, chunkX, chunkY) {
  let dimension = player.publicState.dimension;
  var chunkIdx = [chunkX, chunkY, dimension];

  // Initialise player chunks being monitored collection
  if (typeof player.privateState.chunks === 'undefined')
    player.privateState.chunks = {};

  // Get chunk cursor if it exists
  var cursor = player.privateState.chunks[chunkIdx];
  if (typeof cursor !== 'undefined') {
    delete player.privateState.chunks[chunkIdx];
    cursor.close();
  }
}

// Stop monitoring all chunks for a player
exports.stopMonitoringAll = function(player) {
  if (typeof player.privateState.chunks === 'undefined')
    player.privateState.chunks = {};

  var keys = Object.keys(player.privateState.chunks);
  for (var i = 0; i < keys.length; ++i) {
    var chunkX = parseInt(keys[i].split('_')[0]);
    var chunkY = parseInt(keys[i].split('_')[1]);
    exports.stopMonitoringChunk(player, chunkX, chunkY);
  }

  player.privateState.chunks = {};
}

// Set the owner of a chunk to a player or null
exports.setChunkOwner = function(player, chunkX, chunkY, dimension) {
  getChunk(chunkX, chunkY, dimension).then(function(chunk) {
    chunk.chunkOwner = (player ? player.save_code : null);
    setChunk(chunk);
  })
  .catch(function(err) {
    console.error(err);  
  });
};

// Get the owner of a chunk (null if no owner)
exports.getChunkOwner = function(chunkX, chunkY, dimension) {
  return getChunk(chunkX, chunkY, dimension).then(function(chunk) {
    return chunk.chunkOwner;
  })
  .catch(function(err) {
    console.error(err);  
  });
}

// Try and toggle the owner of a chunk, checking whether the player
// has permission to do so
exports.toggleChunkOwner = function(player, chunkX, chunkY, dimension, cb) {
  var chunk, dimension, dimensionOwner;
  
  getChunk(chunkX, chunkY, dimension)
    .then(function(c) { chunk = c; })
    .then(function() { return getDimension(dimension); })
    .then(function(d) { dimension = d; })
    .then(function() { return players.getPlayer(r.row('save_code').eq(dimension.owner)); })
    .then(function(d) { dimensionOwner = d; })
    .then(function() {
      if (dimension.locked && dimensionOwner && dimensionOwner.save_code != player.save_code) {
        if (dimensionOwner.publicState.friends.indexOf(player.save_code) == 1) {
          throw {errcode: 'err_dimensionowner', params: dimensionOwner.save_code};
        }
      }

      if (chunk.chunkOwner && chunk.chunkOwner != player.save_code) {
        throw {errcode: 'err_chunkowner', params: chunk.chunkOwner};
      }

      if (chunk.chunkOwner == null && player.publicState.chunkCount >= player.publicState.chunkLimit) {
        throw {errcode: 'err_chunklimit', params: chunk.chunkOwner};
      }
    })
    .then(function() {
      if (chunk.chunkOwner == null) {
        chunk.chunkOwner = player.save_code;
        console.log('player ' + player.publicState.name + ' requesting ownership of chunk ' + chunkX + ',' + chunkY);
        setChunk(chunk);
        player.publicState.chunkCount++;
      }
      else {
        chunk.chunkOwner = null;
        console.log('player ' + player.publicState.name + ' relinquishing ownership of chunk ' + chunkX + ',' + chunkY);
        setChunk(chunk);
        player.publicState.chunkCount--;
      }
    })
    .catch(function(e) {
      if (typeof e.errcode == 'undefined') {
        console.error(e);
      }
      else {
        cb(e.errcode, e.params);
      }
    });
}

// Clear all chunks owned by a player
exports.clearChunkOwner = function(save_code) {
  console.log('clearing chunks owned by ' + save_code);
  r.db(db.database).table('map').filter(r.row('chunkOwner').eq(save_code)).run(db.conn)
    .then(function(cursor) {
      return cursor.toArray();
    }).then(function(row) {
      for (var i = 0; i < row.length; ++i) {
        row[i].chunkOwner = null;
        setChunk(row[i]);
      }
    }).catch(function(err) {
      console.error(err);  
    });
}

// Update a blcok (world coordinates) to a new value
function changeBlock(player, x, y, layer, value, cb) {
  var chunkX = Math.floor(x / chunkSize);
  var chunkY = Math.floor(y / chunkSize);

  // javascript does not handle modulo correctly for negative numbers
  var chunkBlockX = (x % chunkSize + chunkSize) % chunkSize;
  var chunkBlockY = (y % chunkSize + chunkSize) % chunkSize;

  var chunk, dimension, chunkOwner, dimensionOwner;

  // todo: the update is nonatomic and slow, since I don't know
  // how to do an atomic update on a nested array in rethinkdb
  getChunk(chunkX, chunkY, player.publicState.dimension)
    .then(function(c) { chunk = c; })
    .then(function() { return getDimension(chunk.dimension); })
    .then(function(d) { dimension = d; })
    .then(function() { return players.getPlayer(r.row('save_code').eq(chunk.chunkOwner)); })
    .then(function(o) { chunkOwner = o; })
    .then(function() { return players.getPlayer(r.row('save_code').eq(dimension.owner)); })
    .then(function(o) { dimensionOwner = o; })
    .then(function() {
      // Check if player has permission to edit this chunk
      if (dimension.locked && dimensionOwner && dimensionOwner.save_code != player.save_code) {
        if (dimensionOwner.publicState.friends.indexOf(player.save_code) == -1) {
          throw {errcode: 'err_dimensionowner', params: dimensionOwner.save_code};
        }
      }
      if (chunkOwner && chunkOwner.save_code != player.save_code) {
        if (chunkOwner.publicState.friends.indexOf(player.save_code) == -1) {
          throw {errcode: 'err_chunkowner', params: chunkOwner.save_code};
        }
      }
    })
    .then(function() {
      if (layer == 'all') {
        for (var i = layers.BACKGROUND_LAYER; i <= layers.OVERLAY_LAYER; ++i) {
          chunk.blocks[i][chunkBlockY * chunkSize + chunkBlockX] = value;
        }
      }
      else {
        chunk.blocks[layer][chunkBlockY * chunkSize + chunkBlockX] = value;
      }
      setChunk(chunk);
    })
    .catch(function(e) {
      if (typeof e.errcode == 'undefined') {
        console.error(e);
      }
      else {
        cb(e.errcode, e.params);
      }
    });
}

// Get chunk data
function getChunk(chunkX, chunkY, dimension) {
  let chunkIdx = [parseInt(chunkX), parseInt(chunkY), dimension];

  return r.db(db.database).table('map')
    .get(chunkIdx)
    .run(db.conn)
    .then(function(chunk) {
      // Create or resize chunk if necessary
      if (chunk == null)
        chunk = {id: chunkIdx, blocks: {}, chunkOwner: null, dimension: dimension};

      // If empty (a new chunk) or doesnt have the right number of blocks
      // OR has too many blocks (why??), fix it
      for (var i = layers.BACKGROUND_LAYER; i <= layers.SOLID_LAYER; ++i) {
        if (!chunk.blocks[i])
          chunk.blocks[i] = [];
        while (chunk.blocks[i].length > chunkSize*chunkSize)
          chunk.blocks[i].pop();
        while (chunk.blocks[i].length < chunkSize*chunkSize)
          chunk.blocks[i].push({tileset: 0, tile: null});
      };
      if (typeof chunk.chunkOwner === 'undefined')
        chunk.chunkOwner = null;
      if (typeof chunk.dimension === 'undefined')
        chunk.dimension = dimension;

      return chunk;
    }).catch(function(err) {
      console.error(err);  
    });
}

// Insert or update chunk
function setChunk(chunk) {
  r.db(db.database).table('map')
    .insert([chunk], {conflict: 'update'})
    .run(db.conn, function(err, res) {
      if (err) throw err;
    });
}

// Send a chunk to a player
function sendChunk(chunk, player) {
  player.socket.emit('chunk_data', chunk);
}

// Get or create a dimension
// Returns a promise with a dimension
// createIfNotExist = true to create a new dimension or false to just return null
// dimension.isNew is true for a new dimension, or false for an existing one
exports.getDimension = getDimension;
function getDimension(dimensionName, createIfNotExist) {
  return r.db(db.database).table('dimensions')
    .get(dimensionName)
    .run(db.conn)
    .then(function(dimension) {
      if (dimension == null && createIfNotExist) {
        // Default dimension
        dimension = { id: dimensionName
                    , owner: null
                    , locked: false
                    };
        setDimension(dimension);
        dimension.isNew = true;
      }
      else if (dimension != null) {
        dimension.isNew = false;
      }

      return dimension;
    })
    .catch(function(e) {
      console.error('error when getting dimension: ' + e);
    });
}

// Write dimension to the database
exports.setDimension = setDimension;
function setDimension(dimension) {
  delete dimension['isNew'];

  return r.db(db.database).table('dimensions')
    .insert([dimension], {conflict: 'update'})
    .run(db.conn)
    .catch(function(e) {
      console.error('error when updating dimension: ' + e);
    });
}

// Set the owner of a dimension
exports.setDimensionOwner = setDimensionOwner;
function setDimensionOwner(dimension, player) {
  getDimension(dimension, true)
    .then(function(dim) {
      dim.owner = player.save_code;
      setDimension(dim);
      players.broadcastServerMessage(player, 'You are now the owner of dimension ' + dimension);
    })
    .catch(function(e) {
      console.error('error when setting dimension owner: ' + e);
    });
}
