const fs = require('fs');

// listen ports - 0 = dont listen on this protocol
const httpPort = 13000;

// whether to instruct clients to reload when the server exits
// this can be used to iterate client development with nodemon
// requires SIGUSR2 so might not work on non-unix platforms
const sendRefreshOnExit = false;

exports.httpPort = httpPort;
exports.sendRefreshOnExit = sendRefreshOnExit;
exports.exports = module.exports;

// If we have a config override, load it
if (fs.existsSync('config.local.js')) {
  require('./config.local.js');
}
